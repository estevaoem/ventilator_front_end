import random
from math import sin


class Facades:
    def __init__(self):
        self.slider = -3.14 / 2
        self.graph_floor = 0

    @staticmethod
    def update_dataget_value(value):
        change = random.randint(0, 1)
        if change:
            value += 1
            return value
        value -= 1
        return value

    def generate_graph_points(self):
        self.graph_floor += 1
        if self.graph_floor > 101:
            self.graph_floor = 1
        return [(x, sin(x / 10.0)) for x in range(0, self.graph_floor)]

    def generate_slider_points(self):
        self.slider += 1 / 100
        if self.slider > 2 * 3.14:
            self.slider = 0
        return 0.4 + 0.4 * sin(self.slider)
