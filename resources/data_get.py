def update_data_get(widget, data_dict):
    for data in widget.data_get:
        try:
            widget.data_get[data]["reference"].text = str(round(data_dict[data], 1))
        except:  # pylint:disable=bare-except
            widget.data_get[data]["reference"].text = "-.-"
