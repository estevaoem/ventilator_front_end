from queue import Queue
import time

pararameter_queue = Queue()
par = {}


def set_popup_value(widget, direction, buttons):
    sleep_time = 0.2
    if direction == "right":
        while True:
            widget.data_set[widget.hover_btn]["value"] += widget.data_set[widget.hover_btn]["step"]
            widget.popup.content.children[
                1
            ].text = f"[b]{widget.data_set[widget.hover_btn]['value']:{widget.data_set[widget.hover_btn]['print_format']}} {widget.data_set[widget.hover_btn]['unit']}[/b]"
            time.sleep(sleep_time)
            if (sleep_time>0.05):
                sleep_time-=0.01
            if buttons.getState("right"):
                break

    if direction == "left":
        while True:
            widget.data_set[widget.hover_btn]["value"] -= widget.data_set[widget.hover_btn]["step"]
            widget.popup.content.children[
                1
            ].text = f"[b]{widget.data_set[widget.hover_btn]['value']:{widget.data_set[widget.hover_btn]['print_format']}} {widget.data_set[widget.hover_btn]['unit']}[/b]"
            time.sleep(sleep_time)
            if (sleep_time>0.05):
                sleep_time-=0.01
            if buttons.getState("left"):
                break

    if direction == "up":
        widget.data_set[widget.hover_btn][
            "reference"
        ].text = f"[b]{widget.data_set[widget.hover_btn]['value']:{widget.data_set[widget.hover_btn]['print_format']}} {widget.data_set[widget.hover_btn]['unit']}[/b]"
        par[widget.hover_btn] = widget.data_set[widget.hover_btn]["value"]
        try:
            pararameter_queue.put(par)
        except:  # pylint:disable=bare-except
            pass
        widget.update = True
        widget.popup.dismiss()



def set_hover(widget, direction):
    def change_hover(id):
        for data in widget.data_set:
            if widget.data_set[data]["id"] == id:
                widget.hover_btn = data
                widget.remove_widget(widget.hover_btn_lbl)
                widget.hover_btn_lbl.pos_hint["x"] = (
                    1920 - 1189
                ) / 1920 + id * 198.183 / 1920
                widget.add_widget(widget.hover_btn_lbl, index=-1)
                break

    if direction == "right":
        id = widget.data_set[widget.hover_btn]["id"] + 1
        id = check_id(id)
        change_hover(id)

    elif direction == "left":
        id = widget.data_set[widget.hover_btn]["id"] - 1
        id = check_id(id)
        change_hover(id)

    elif direction == "up":
        widget.keyboard.target = widget.popup
        widget.update = False
        widget.is_popup_open = True
        widget.open_popup()


def update_data_set(widget, data_dict):
    if widget.update:
        for data in widget.data_set:
            try:
                if data is not "vt":
                    value = data_dict[data]
                else:
                    value = round(data_dict[data]/10,0)*10
                widget.data_set[data]["value"] =value
                widget.data_set[data]["reference"].text = f"{value:{widget.data_set[data]['print_format']}} {widget.data_set[data]['unit']}"
            except:  # pylint:disable=bare-except
                widget.data_set[data]["reference"].text = "-.-"


def check_id(id):
    if id > 5:
        return 0
    if id < 0:
        return 5
    return id
