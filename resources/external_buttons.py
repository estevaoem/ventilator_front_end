import RPi.GPIO as GPIO

class ExternalButtons:
    _pins = {}
    def __init__(self, LEFT, RIGHT, ENTER, LOCK, ALARM):
        self._pins = {"left": LEFT, "right": RIGHT, "enter":ENTER, "lock":LOCK,"alarm":ALARM}
        # _callbacks = {"left": None, "right": None, "enter":None, "lock":None,"alarm":None}
        self.setup()

    def setup(self):
        GPIO.setmode(GPIO.BCM)
        for pin in self._pins:
            GPIO.setup(self._pins[pin], GPIO.IN, pull_up_down=GPIO.PUD_UP)

    def setCallback(self, button, callback):
        if button == "left" or button == "right" or button == "enter" or button == "lock" or button == "alarm":
            GPIO.add_event_detect(self._pins[button], GPIO.FALLING)
            GPIO.add_event_callback(self._pins[button], callback)
        else:
            raise("wrong button argument")

    def getState(self, button):
        if button == "left" or button == "right" or button == "enter" or button == "lock" or button == "alarm":
            return GPIO.input(self._pins[button])
        else:
            raise("wrong button argument")



buttons = ExternalButtons(LEFT=17, RIGHT=22, ENTER=27, LOCK=10, ALARM=9)
