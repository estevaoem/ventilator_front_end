from kivy_garden.graph import Graph, LinePlot


def add_point(old_points, new_value, max_points):
    new_points = old_points
    if len(new_points) > max_points:
        new_points = [(i, new_points[i + 1][1]) for i in range(max_points - 1)]
    new_points.append([len(new_points) - 1, new_value])
    return new_points


def update_graph_points(widget, data_dict):
    old_points = widget.plot.points
    widget.clear_widgets()
    try:
        new_points = add_point(old_points, data_dict[widget.quantity], 100)
        if data_dict[widget.quantity] > widget.graph.ymax:
            widget.graph.ymax = int(data_dict[widget.quantity]/10)*10 + 10
            widget.graph.y_ticks_major = (widget.graph.ymax  - widget.graph.ymin)/10
        if data_dict[widget.quantity] < widget.graph.ymin:
            widget.graph.ymin = int(data_dict[widget.quantity]/10)*10 - 10
            widget.graph.y_ticks_major = (widget.graph.ymax  - widget.graph.ymin)/10
    except:  # pylint:disable=bare-except
        new_points = add_point(old_points, 0, 100)

    widget.plot.points = new_points
    widget.add_widget(widget.graph)


def create_graph(
    inp_xlabel, inp_ylabel, y_max, y_min, color=[0, 1, 0, 1]
):  # pylint: disable=dangerous-default-value
    graph = Graph(
        xlabel=inp_xlabel,
        ylabel=inp_ylabel,
        x_ticks_minor=5,
        x_ticks_major=25,
        y_ticks_major=y_max / 10,
        y_grid_label=True,
        x_grid_label=True,
        padding=5,
        x_grid=True,
        y_grid=True,
        xmin=-0,
        xmax=100,
        ymin=y_min,
        ymax=y_max,
    )
    plot = LinePlot(color=color, line_width=1.5)

    return graph, plot
