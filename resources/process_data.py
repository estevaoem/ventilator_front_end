processed_data = {
    "pressure": 0 or "-.-",
    "flow": 0 or "-.-",
    "volume": 0 or "-.-",
    "fio2": 0 or "-.-",
    "vt": 0 or "-.-",
    "fr": 0 or "-.-",
    "vm": 0 or "-.-",
    "tinsp": 0 or "-.-",
    "texp": 0 or "-.-",
    "tpi": 0 or "-.-",
    "ie": 0 or "-.-",
    "ppeak": 0 or "-.-",
    "ppl": 0 or "-.-",
    "peep": 0 or "-.-",
    "qpico": 0 or "-.-",
}


def process_data(data_dict):
    for data in data_dict:
        processed_data[data] = data_dict[data]
    return processed_data
