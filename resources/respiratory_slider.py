def update_respiratory_slider(widget, data_dict):
    if widget.peek_pressure < data_dict["pressure"]:
        widget.peek_pressure_red_mark.pos_hint["y"] = (
            data_dict["pressure"] * 15.9591333333 / 1080 + 294.610 / 1080
        )
        widget.peek_pressure_lbl.pos_hint["y"] = (
            data_dict["pressure"] * 15.9591333333 / 1080 + 288.124 / 1080
        )
        widget.peek_pressure = data_dict["pressure"]

    widget.green_slider.size_hint[1] = data_dict["pressure"] * 15.9591333333 / 1080
    widget.peep_red_mark.pos_hint["y"] = (
        data_dict["peep"] * 15.9591333333 / 1080 + 294.610 / 1080
    )
    widget.peep_lbl.pos_hint["y"] = (
        data_dict["peep"] * 15.9591333333 / 1080 + 288.124 / 1080
    )
    widget.ppl_red_mark.pos_hint["y"] = (
        data_dict["ppl"] * 15.9591333333 / 1080 + 294.610 / 1080
    )
    widget.ppl_lbl.pos_hint["y"] = (
        data_dict["ppl"] * 15.9591333333 / 1080 + 288.124 / 1080
    )
