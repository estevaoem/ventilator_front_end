import json
from threading import Thread

import serial
import time


class SerialHandler:
    def __init__(self, pararameter_queue=None):
        self.ser = serial.Serial("/dev/serial0", 115200)
        self.ser.timeout = 1
        self._dataqueues = []
        self._parameterqueue = pararameter_queue
        self.sh_thread = None

    def send_data(self):
        try:
            data2send = self._parameterqueue.get()
            self.ser.write(f"{json.dumps(data2send)}\n".encode())
            self.dispatch(json.loads(self.ser.readline()))
        except Exception as e:  # pylint:disable=broad-except
            print(str(e))

    def register_queue(self, queue):
        self._dataqueues.append(queue)
        return queue

    def dispatch(self, data):
        for queue in self._dataqueues:
            queue.put(data)

    def method(self):
        while True:
            try:
                self.ser.write(f"D\n".encode())
                while self.ser.in_waiting:
                    self.dispatch(json.loads(self.ser.readline()))
                if self._parameterqueue.qsize() > 0:
                    self.send_data()
                    #print(self.ser.readline())
            except:  # pylint:disable=bare-except
                pass
            time.sleep(0.01)

    def run(self, **kwargs):
        self.sh_thread = Thread(target=self.method, kwargs=kwargs)
        self.sh_thread.start()
