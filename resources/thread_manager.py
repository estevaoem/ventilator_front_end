import random
from queue import Queue
from threading import Thread
from time import sleep

from resources.data_set import pararameter_queue
from resources.process_data import process_data
from resources.serial_handler import SerialHandler

# no problems using this:
# https://medium.com/better-programming/alternatives-to-using-globals-in-python-a3b2a7d5411b

threads = []


def thread_starter(queue):
    while True:
        # Uncomment the next three lines for populating the data_queue in case of serialPort being unavailable
        # demo_data = {
        #     "peep": random.uniform(4, 9),
        #     "pressure": random.uniform(5, 30),
        #     "flow": random.uniform(10, 60),
        #     "volume": random.uniform(100, 500),
        # }
        # demo_data["ppl"] = demo_data["pressure"] * 0.3
        # queue.put(demo_data)
        data_dict = process_data(queue.get())
        for request in threads:
            request[0](request[1], data_dict)
            # sleep(0.01)


data_queue = Queue()
s = SerialHandler(pararameter_queue=pararameter_queue)
s.register_queue(data_queue)
s.run()

update_thread = Thread(target=thread_starter, args=(data_queue,))
