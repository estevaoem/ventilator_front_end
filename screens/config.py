from kivy.config import Config
from kivy.core.window import Window


def set_default_configuration():
    Window.fullscreen = True
    Window.size = (1366, 768)
    # Window.size = (1920, 1080)
    # Window.borderless = "1"
    # Window.clearcolor = (1, 22, 39, 1)

    Config.set("input", "mouse", "mouse,multitouch_on_demand")
