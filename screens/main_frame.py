# from resources.external_buttons import encoder


import os

from kivy.app import App
from kivy.core.window import Window
from kivy.properties import ObjectProperty  # pylint:disable=no-name-in-module
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.popup import Popup

import screens.config as sc
from resources.data_get import update_data_get
from resources.data_set import set_hover, set_popup_value, update_data_set
from resources.graphs import create_graph, update_graph_points
from resources.respiratory_slider import update_respiratory_slider
from resources.thread_manager import threads, update_thread
from screens.popupcontent import PopUpContent
from resources.external_buttons import buttons

sc.set_default_configuration()


class MainFrame(App):
    def build(self):
        return MainScreen()


class MainScreen(FloatLayout):
    @staticmethod
    def start_update_thread():
        update_thread.start()


class RespiratorySlider(FloatLayout):
    def __init__(self, **kwargs):
        super(RespiratorySlider, self).__init__(**kwargs)
        self.peek_pressure = 0

    green_slider = ObjectProperty(None)
    peek_pressure_red_mark = ObjectProperty(None)
    peep_red_mark = ObjectProperty(None)
    peek_pressure_lbl = ObjectProperty(None)
    peep_lbl = ObjectProperty(None)
    ppl_red_mark = ObjectProperty(None)
    ppl_lbl = ObjectProperty(None)

    def updater(self):
        request = [update_respiratory_slider, self]
        threads.append(request)


class DataGet(FloatLayout):
    def __init__(self, **kwargs):
        super(DataGet, self).__init__(**kwargs)
        self.data_get = None

    get_fio2 = ObjectProperty(None)
    get_vt = ObjectProperty(None)
    get_fr = ObjectProperty(None)
    get_vm = ObjectProperty(None)
    get_tinsp = ObjectProperty(None)
    get_texp = ObjectProperty(None)
    get_tpl = ObjectProperty(None)
    get_ie = ObjectProperty(None)
    get_ppico = ObjectProperty(None)
    get_ppl = ObjectProperty(None)
    get_peep = ObjectProperty(None)
    get_qpico = ObjectProperty(None)

    def build_data_get(self):
        self.data_get = {
            "fio2": {"id": 0, "value": 60, "reference": self.get_fio2, "unit": "%",},
            "vt": {"id": 1, "value": 500, "reference": self.get_vt, "unit": "mL",},
            "fr": {"id": 2, "value": 1.5, "reference": self.get_fr, "unit": "s",},
            "vm": {"id": 3, "value": 7.5, "reference": self.get_vm, "unit": "L/min",},
            "tinsp": {"id": 4, "value": 1.5, "reference": self.get_tinsp, "unit": "s",},
            "texp": {"id": 5, "value": 2.5, "reference": self.get_texp, "unit": "s",},
            "tpl": {"id": 6, "value": 1.5, "reference": self.get_tpl, "unit": "s",},
            "ie": {"id": 7, "value": 0.6, "reference": self.get_ie, "unit": "s",},
            "ppeak": {
                "id": 8,
                "value": 32,
                "reference": self.get_ppico,
                "unit": "cm H[sub]2[/sub]O",
            },
            "ppl": {
                "id": 9,
                "value": 27,
                "reference": self.get_ppl,
                "unit": "cm H[sub]2[/sub]O",
            },
            "peep": {
                "id": 10,
                "value": 5,
                "reference": self.get_peep,
                "unit": "cm H[sub]2[/sub]O",
            },
            "qpico": {
                "id": 11,
                "value": 20,
                "reference": self.get_qpico,
                "unit": "L/min",
            },
        }
        for data in self.data_get:
            self.data_get[data][
                "reference"
            ].text = (
                f"[b]{self.data_get[data]['value']} {self.data_get[data]['unit']}[/b]"
            )

    def updater(self):
        request = [update_data_get, self]
        threads.append(request)


class DataSet(FloatLayout):
    def __init__(self, **kwargs):
        super(DataSet, self).__init__(**kwargs)
        self.popup = Popup()
        self.content = PopUpContent()
        self.data_set = None
        self.keyboard = Window.request_keyboard(callback=None, target=self)
        self.hover_btn = "fio2"
        # self.hover_btn_pos = None
        self.is_popup_open = False
        self.update = True

    set_fio2 = ObjectProperty(None)
    set_vt = ObjectProperty(None)
    set_fr = ObjectProperty(None)
    set_tinsp = ObjectProperty(None)
    set_peep = ObjectProperty(None)
    set_tpl = ObjectProperty(None)
    set_peak = ObjectProperty(None)
    hover_btn_lbl = ObjectProperty(None)

    def updater(self):
        request = [update_data_set, self]
        threads.append(request)

    def build_data_set(self):
        buttons.setCallback("right",self.right)
        buttons.setCallback("left",self.left)
        buttons.setCallback("enter",self.enter)
        self.keyboard.bind(on_key_up=self.keyboard_pressing)
        self.data_set = {
            "fio2": {
                "id": 0,
                "value": 62,
                "reference": self.set_fio2,
                "unit": "%",
                "step": 1,
                "print_format": ".0f",
                "markup": "FiO[sub]2[/sub]",
            },
            # "vt": {
            #     "id": 1,
            #     "value": 53,
            #     "reference": self.set_vt,
            #     "unit": "mL",
            #     "step": 10,
            #     "print_format": ".0f",
            #     "markup": "V[sub]T[/sub]",
            # },
            "ppeak": {
                "id": 1,
                "value": 30,
                "reference": self.set_ppeak,
                "unit": "cm H[sub]2[/sub]O",
                "step": 1,
                "print_format": ".0f",
                "markup": "P[sub]pico[/sub]",
            },
            "fr": {
                "id": 2,
                "value": 65,
                "reference": self.set_fr,
                "unit": "rpm",
                "step": 1,
                "print_format": ".0f",
                "markup": "F[sub]R[/sub]",
            },
            "tinsp": {
                "id": 3,
                "value": 32,
                "reference": self.set_tinsp,
                "unit": "s",
                "step": 0.1,
                "print_format": ".1f",
                "markup": "T[sub]insp[/sub]",
            },
            "peep": {
                "id": 4,
                "value": 43,
                "reference": self.set_peep,
                "unit": "cm H[sub]2[/sub]O",
                "step": 1,
                "print_format": ".0f",
                "markup": "PEEP",
            },
            "tpl": {
                "id": 5,
                "value": 66,
                "reference": self.set_tpl,
                "unit": "s",
                "step": 0.1,
                "print_format": ".1f",
                "markup": "T[sub]pi[/sub]",
            },
        }
        self.popup.on_dismiss = self.set_popup_dismiss
        for data in self.data_set:
            self.data_set[data][
                "reference"
            ].text = (
                f"[b]{self.data_set[data]['value']} {self.data_set[data]['unit']}[/b]"
            )

    def open_popup(self):
        self.content.gui_variable = self.data_set[self.hover_btn]["markup"]
        self.content.gui_value = self.data_set[self.hover_btn]["value"]
        self.content.gui_unit = self.data_set[self.hover_btn]["unit"]
        self.content.build_popup_content()
        self.set_popup()
        self.popup.open()

    def set_popup(self):
        self.popup.background_color = (0, 0, 0, 0.9)
        self.popup.background = str(os.getcwd()) + "/asserts/popup_bg.png"
        self.popup.title = ""
        self.popup.separator_height = 0
        self.popup.content = self.content
        # for child in self.popup.children:
        #     print(child)
        #     print("")
        #     for childin in child.children:
        #         print(childin)
        #         if isinstance(childin, BoxLayout):
        #             for cria in childin.children:
        #                 print("###")
        #                 print(cria)
        #                 print("###")
        #                 for prole in cria.children:
        #                     print("¬¬")
        #                     print(prole)
        #                     print("¬¬")
        self.popup.size_hint = (600 / 1366, 350 / 768)

    def set_popup_dismiss(self):
        self.keyboard.target = self
        self.is_popup_open = False

    def left(self,pin):
        if self.is_popup_open:
            set_popup_value(self, "left", buttons)
        else:
            set_hover(self, "left")

    def right(self,pin):
        if self.is_popup_open:
            set_popup_value(self, "right", buttons)
        else:
            set_hover(self, "right")

    def enter(self,pin):
        if self.is_popup_open:
            set_popup_value(self, "up", buttons)
            # self.update = False
        else:
            set_hover(self, "up")

    def keyboard_pressing(self, keyboard, keycode):  # pylint:disable=unused-argument
        if self.is_popup_open:
            if keycode[1] == "w":
                set_popup_value(self, "up")

            elif keycode[1] == "a":
                set_popup_value(self, "left")

            elif keycode[1] == "d":
                set_popup_value(self, "right")
        else:
            if keycode[1] == "w":
                set_hover(self, "up")

            elif keycode[1] == "a":
                set_hover(self, "left")

            elif keycode[1] == "d":
                set_hover(self, "right")


class ScreenGraph(BoxLayout):
    def __init__(self, **kwargs):
        super(ScreenGraph, self).__init__(**kwargs)
        self.graph = None
        self.plot = None
        self.plot_floor = 1

    def build_graph(self, xlabel="", ylabel="", y_max=100, y_min=0, color=[0, 1, 0, 1]):
        self.graph, self.plot = create_graph(
            inp_xlabel=xlabel, inp_ylabel=ylabel, y_max=y_max, y_min=y_min, color=color
        )
        self.plot.points = [[0, 0]]
        self.graph.add_plot(self.plot)

    def updater(self):
        request = [update_graph_points, self]
        threads.append(request)
