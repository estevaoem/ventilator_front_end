from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label


class PopUpContent(BoxLayout):
    def __init__(self, **kwargs):
        super(PopUpContent, self).__init__(**kwargs)
        self.gui_variable = None
        self.gui_value = None
        self.gui_unit = None
        self.orientation = "vertical"

    def build_popup_content(self):
        self.clear_widgets()
        gui_variable = Label(
            text=f"[b]{self.gui_variable}[/b]", markup=True, halign="center",
        )
        gui_variable.font_size = gui_variable.height

        gui_value = Label(
            text=f"[b]{self.gui_value} {self.gui_unit}[/b]",
            markup=True,
            halign="center",
        )
        gui_value.font_size = gui_value.height * 0.9
        hint = Label(
            text="[b]Selecione o valor desejado e \r\n pressione Enter para configurar[/b]", markup=True, halign="center",
        )
        hint.font_size = hint.height * 0.35

        self.add_widget(gui_variable)
        self.add_widget(gui_value)
        self.add_widget(hint)
